function pars = getScalePars(pars)
% 2020 by Jan Klanke
% 
% Input:  pars - parameters that are needed for the scale (mostly sizes in dva)
%
% Output: pars - additional and modified parameters (modtly sizes in
%                pixels but also text options and such)

global scr setting

% little helper function
paren   = @(x, varargin) x(varargin{:});

% Appearance Parameters.
numBands = pars.itemN-1;                           % number of bands that make up the scale face
bandCol1 = .7 * scr.white;                         % color, expressed as monitor luminance
bandCol2 = .3 * scr.white;                         % color, expressed as monitor luminance      
colsVect = repelem([bandCol1,bandCol2],2);         % combine colors into vector
pars.bandColV  = repmat(colsVect(mod(0:2*numBands - 1,numel(colsVect)) + 1),3,1); % combine color values into vector
pars.widthP    = round(pars.width*scr.ppd);        % width of the scale-line [pix]
pars.handSizeD = pars.width*3;                     % length of clockhand [dva]
pars.handSizeP = round(pars.handSizeD * scr.ppd);  % length of clockhand [dva]
pars.Dir       = setting.sdir;                     % direction of the scales

% Movement parameters.
pars.Speed = scr.refr * 2;                         % travel speed of the response point on the likert scale (in vector units)  

% Spatial stuff for the line.
lposXD  = linspace(-pars.length/2, pars.length/2, pars.itemN);
lposXDv = paren(repelem(lposXD,2), 2:length(repelem(lposXD,2))-1);
pars.line.posD = [lposXDv; zeros(1, length(lposXDv))];
pars.line.posP = round(pars.line.posD * scr.ppd);

% Spatial stuff for the scale hand
pars.hand.posvecD = [linspace(-pars.length/2,pars.length/2,pars.Speed);zeros(1,pars.Speed)];
pars.hand.posvecP = round(pars.hand.posvecD*scr.ppd);

% Text stuff for the 'question'.
pars.lvl(1).q.labels = {'Did you perceive the'; 'STIMULUS FLASH?'};
pars.lvl(2).q.labels = {'Do you think you generated a'; 'CATCH-UP SACCADE?'};
pars.lvl(3).q.labels = {'How sure are you that the stimulus'; 'flash WAS caused by the catch-up saccade?'};
pars.lvl(4).q.labels = {'How sure are you that the stimulus'; 'flash was NOT caused by the catch-up saccade?'};

% Text stuff for the scale text.
for i= 1:2;pars.lvl(i).a.labels = {'No!','Yes!'}; if ~pars.Dir; pars.lvl(i).a.labels = fliplr(pars.lvl(i).a.labels);end;end
for i= 3:4;pars.lvl(i).a.labels = {'not','rather not','rather','very';'sure.','sure.','sure.','sure.'};if ~pars.Dir; pars.lvl(i).a.labels = fliplr(pars.lvl(i).a.labels);end;end

% Spatial stuff for text.
seli = {[2 6]; [2 6]; [1:7]; [1:7]}; % indeces of spatial vector entries to be used per level 
getqpos = @(x)[zeros(length(x),1), -.75 + cumsum(-.75*ones(size(x)), 'reverse')];
getapos = @(x,y)[repmat(linspace(min(y),max(y),length(x)),1,size(x,1))',reshape(.5 + cumsum(.75*ones(size(x)),1)',1,[])'];
for i= 1:length(pars.lvl)
    pars.lvl(i).q.posD = getqpos(pars.lvl(i).q.labels);                 pars.lvl(i).q.posP = round(pars.lvl(i).q.posD*scr.ppd);
    pars.lvl(i).a.posD = getapos(pars.lvl(i).a.labels,lposXD(seli{i})); pars.lvl(i).a.posP = round(pars.lvl(i).a.posD*scr.ppd);
end

end