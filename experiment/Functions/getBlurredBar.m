function sti = getBlurredBar(par)
% 2021 by Martin Rolfs
% 2021 by Jan Klanke
% 
% Input:  par - parameters that are needed for stimulus (i.e. sine
%               grating and the raised cosine mask)
%
% Output: sti - Matrix (sineGrating : sineGrating : 2D-RaisedCosineMask)

global scr 

%%%%%%%%%%%%%%%%%%%%%%%
% Define sine grating %
%%%%%%%%%%%%%%%%%%%%%%%

% Calculate parameters of the grating:
ppc = ceil(1/par.frqp)*2;                 % pixels/cycle, rounded up.
sizX = scr.resX * 10 + ~mod(scr.resX,2);   % extent of the grating in the horizontal plane (equal to 2*screen size in pix).
sizY = par.lenp/2 + ~mod(par.lenp/2,2);   % siz in pixels, rounded up (if necessary)

% Create one single static grating image:
X = meshgrid(-sizX:sizX + ppc,-sizY:sizY);
grating = scr.bgColor + scr.bgColor*par.amp*cos(X*par.frqp*2*pi + par.pha);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define raised cosine mask %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% outer part
len = par.lenp - mod(par.lenp,2);      % bar length in pixels (make sure its an even number)
hig = par.higp - mod(par.higp,2);      % bar height in pixels (make sure its an even number)
R   = par.tap;                         % length of the taper section (outer)

% get raised cosine Functions for filter 
base = raisedCos(len, hig, R);

% reduce contrast of the max by half.
mask = base;

% store pulse-shaped filter mask in 2nd channel of grating.
% grating = [];
grating(:,:,2) = 0;
grating(1:par.higp-mod(par.higp,2), 1:par.lenp-mod(par.lenp,2), 2) = mask;

% make grating the ouput stimulus
sti = grating;
end

% function that calcualtes the raised fading 
function base = raisedCos(len, hig, R)

tukeyHig= tukeywin(hig,R);
tukeyLen= tukeywin(len,R);   
baseHig = repmat(tukeyHig,  1,   len); 
baseLen = repmat(tukeyLen', hig, 1  ); 
% base   =  (baseHig + baseLen)/2;
base    = baseHig .* baseLen;


end
