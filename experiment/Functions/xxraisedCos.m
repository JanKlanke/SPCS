% function that calcualtes the raised fading 
function base = xxraisedCos(len, hig, R)

tukeyHig= tukeywin(hig,R);
tukeyLen= tukeywin(len,R*(hig/len));   
baseHig = repmat(tukeyHig,  1,   len); 
baseLen = repmat(tukeyLen', hig, 1  ); 
% base    = baseHig .* baseLen;
base    = (baseHig + baseLen)./2;

end