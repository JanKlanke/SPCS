function sac = getMsData(ms, id)
% 2020 by Jan Klanke
% 
% Input:  ms  - table with microsaccade data (identifier and gaze positions). 
%
% Output: sac - vector with coordinates, velocities, and onset time points
%               (etc.) of microsaccade data and retinal positions


% get necessary mini-function
paren = @(x, varargin) x(varargin{:});

% return to main script if ms data is not available (i.e. in 1st session)
sac=[];

if ~any(structfun(@isempty, ms))
    
    % load coordinates and velocities for the replay
    [~, ~, idx] = unique(ms.info(:,1:4), 'rows');
    for i = 1:max(idx)
        
        % get MS id
        sac(i).idtext = sprintf( '%s%02d%02d%03d_%i', id, paren(ms.info(idx==i,1),1), paren(ms.info(idx==i,2),1), paren(ms.info(idx==i,3),1), paren(ms.info(idx==i,4),1));
        
        % get raw coordinates and velocities (x and y)
        sac(i).coordsMS = ms.info(idx==i, 7:8)';        % load in the x coordinates of the MS [dva]
        sac(i).velosMS  = ms.info(idx==i, 10:11)';
        
        % calculte 1D position vector and the velocity along it
        sac(i).cVectorMS = sqrt(sac(i).coordsMS(1,:).^2 + sac(i).coordsMS(2,:).^2);
        sac(i).vVectorMS = diff(sac(i).cVectorMS);
        
        % get coordinates and velocities (x and y) of RETINAL CONSEQUENCES
        sac(i).coordsRC = ms.info(idx==i, 12:13)';       % load in the x coordinates of the MS [dva]
        sac(i).velosRC  = ms.info(idx==i, 14:15)';
        
        % calculte 1D position vector and the velocity along it
        sac(i).cVectorRC = sqrt(sac(i).coordsRC(1,:).^2 + sac(i).coordsRC(2,:).^2);
        sac(i).vVectorRC = diff(sac(i).cVectorRC);
        
        % load onsets of the MS in ms and frames
        sac(i).onsetMS = ms.info(find(idx==i, 1,'first'),5)';
    end

end 

end