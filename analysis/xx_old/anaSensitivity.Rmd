---
title: |
       | JSEP Analysis Script 2:
       | Stimulus sensitivity.
author: Jan Klanke
date: "`r Sys.Date()`"
output: 
  pdf_document: default
---

```{r setup, include= FALSE}
# Set stuff for document.
knitr::opts_chunk$set( include= TRUE, size= 'scriptsize' )
knitr::opts_knit$set(  root.dir= '/Users/jan' )

# Change hook to be able to modify font size in chunks
def.source.hook <- knitr::knit_hooks$get( 'source' )
knitr::knit_hooks$set( source= function( x, options ) {
  x <- def.source.hook( x, options )
  ifelse( !is.null( options$size ), 
         paste0( '\\', options$size, '\n\n', x, '\n\n \\normalsize' ), 
         x ) } )
```

#Preparations.
##Overall.
Here go all the parameters needed for the preprocessing of the script: Mainly name of the experiment and the functions needed in the different subsequent sections (should be pretty self-explanatory?).
```{r}
# Name of the experiment.
expname <- c( 'PSSR', 'JSEP' )
date    <- format( Sys.Date(), '%y%m%d' ) 

# Do you want to exclude the first session.
exclude1st <- 'yes'

# Do you wanna save all your plots?
savePlots <- 'yes'
```

##Folder & format spec. parameter
Variables for the relacant paths (f.e. where is the data stored, where will the output be stored) and format specifications.
```{r}
# Directory of subfolder with data. 
fld <- list()
fld$dataRepo  <- './Seafile/Data/'                                        # Directory where the data can be found  
fld$processed <- paste0( fld$dataRepo, expname, '/preprocessed/' )       # (i.e. './Documents/ExpName/processed/') 
fld$trial <- paste0( fld$dataRepo, expname, '/preprocessed/trialData' )  # (i.e. './Documents/ExpName/processed/trial/') 
fld$msc   <- paste0( fld$dataRepo, expname, '/preprocessed/msCoordinates' )

# Directory of subfolder with functions for MS detection 
fld$functions <- './Documents/pssr/analysis/functions/'                  # (i.e. './Documents/ExpName/prepare/functions) 
fld$figures   <- './Documents/jsep/analysis/figures/vis/'                    # (i.e. './Documents/ExpName/processed/trial/') 

# Format specs of input and ouptu data (likely no need for change!).
fS <- list()
fS$csv <- '.csv'      # file format of destination data
fS$pdf <- '.pdf'      # file format of saccade plots
```

##Functions.
```{r}
# Define function for calculating 95% CI.
confInt <- function( x, v ) { ( qnorm( .975 ) * ( sd ( x, na.rm= TRUE ) / sqrt( n() ) ) ) }

# Define functions needed for the circular calculations:
rad2deg <- function( ang ) { ( ang * 180 ) / ( pi  ) }                       # - recalculates radians in degrees
deg2rad <- function( ang ) { ( ang * pi  ) / ( 180 ) }                       # - recalculates radians in degrees
normalizeDeg <- function( ang ) { -( - ( ang + 180 ) %% 360 ) + 180 }        # - normalizes degree to max 360 degree
absDiffDeg   <- function( a, b ) { abs( normalizeDeg( normalizeDeg( a ) - normalizeDeg( b ) ) ) } # - calculate the absolute diff. between normalized angles
normalizeRan <- function( ang ){ ifelse( ang > 180, 360 - ang, ang ) }       # - normalize (again) to 180 degree.

# Define function for calculating the main sequence 
Collewijn89_func <- function( V0, A0, amp ) { V0 * ( 1 - exp( -amp / A0 ) ) }

# Functions that helps creating semi-circular points.
circleFun <- function( center= tibble( x= 0, y= 0 ), diameter= 1, npoints= 100, start= 0, end= 2 ) {
  tt <- seq( start * pi, end * pi, length.out= npoints )
  tb <- tibble( id= rep( seq( 1, center %>% nrow(), 1 ), each= npoints ) )%>% 
    bind_cols( center[1] %>% slice( rep( 1:n(), each= length( tt ) ) ) + diameter / 2 * cos( tt ),
               center[2] %>% slice( rep( 1:n(), each= length( tt ) ) ) + diameter / 2 * sin( tt ) ) %>% 
    rename_all( ~c( 'id', 'x', 'y' ) )
  return( tb ) }
```

##Graphical parameters.
```{r message= FALSE, warning= FALSE}
# Organize theme.
thematic::thematic_on()
ggplot2::theme_minimal() + 
  ggplot2::theme( strip.text= ggplot2::element_text( size= 18, family= 'Calibri' ), 
                  axis.title= ggplot2::element_text( size= 18, family= 'Calibri' ), 
                  axis.text = ggplot2::element_text( size= 12, family= 'Calibri' ), 
                  legend.title= ggplot2::element_blank(),
                  legend.text = ggplot2::element_text( size= 12, family= 'Calibri' ), 
                  strip.placement= 'outside', legend.position= 'none' ) -> generalTheme

# Labels for the plot
c( 'noStim', 'sim_replay', 'sim_active', 'real_replay','real_active' ) -> conditionNames
c( '0MS', '1MS', 'incongruent', 'congruent', 'dissimilarVels', 'similarVels', 'incongruent*dissimilarVels', 
  'incongruent*similarVels', 'congruent*dissimilarVels', 'congruent*similarVels' ) -> lev

# Colors
colsRep <- colorRampPalette( RColorBrewer::brewer.pal( length( lev ), 'YlOrRd' )[2:9] )(9)
colsAct <- colorRampPalette( RColorBrewer::brewer.pal( length( lev ), 'YlGnBu' )[2:9] )(10)
cols <- c( colsAct, colsRep )

# Also: register custom-fonts with rendering device
extrafont::loadfonts( device= 'pdf', quiet=TRUE )   
```


#Loading.
##Load libraries & sources.
```{r results= 'hide', fig.show= 'hide', message= FALSE, warning= FALSE, include= FALSE}
# Load libraries.
libraries <- c( 'data.table', 'tidyverse', 'vroom', 'RColorBrewer', 'Cairo', 'extrafont' )
lapply( libraries, require, character.only= TRUE )
```

##Load data files.
Check which files are in the preprocessed folder (fList$processed), and load all files in ONE data frame (for easier processing).
```{r message= FALSE, warning= FALSE}
# Get names of MSG files from df dir.
fList <- list()
list.files( fld$processed, paste0( '*', fS$csv ), full.names= TRUE )   -> fList$processed
list.files( fld$msc, paste0( 'msCoords*', fS$csv ), full.names= TRUE ) -> fList$msCoords


# Load MSG files.
vroom( fList$processed[ str_detect( fList$processed, 'PSSR' ) ], col_names= TRUE, delim= ',', id= 'Experiment' ) -> df_rawPSSR 
vroom( fList$processed[ str_detect( fList$processed, 'JSEP' ) ], col_names= TRUE, delim= ',', id= 'Experiment' ) %>% 
  mutate( seno= as.character( seno ) ) -> df_rawCSEP 
bind_rows( df_rawPSSR, df_rawCSEP ) %>% 
  mutate( Experiment= str_remove_all( Experiment, '.*/|_.*' ) ) %>% 
  filter( !str_detect( Experiment, 'CSOP' ) ) -> df_raw
vroom( fList$msCoords[ str_detect( fList$msCoords, 'PSSR' ) ], col_names= TRUE, delim= ',' ) %>% 
  unite( 'tIDrepSac', vpno:trial, sep= '', remove= FALSE ) %>% 
  distinct( vpno, tIDrepSac ) %>% 
  group_by(  vpno ) %>% 
  mutate( dispSacIdx= 1:n() ) -> df_msCoordIdx
left_join( df_raw, df_msCoordIdx, by= c( 'vpno', 'dispSacIdx' ) ) -> df_raw
```


#Initial data manipulations.
##Translate, & filter data.
Beginn by recoding the most important variables so as to make the calculations and the plotting easier (recode := convert to strings). These variables include experimental condition, session no., categorical output. Similarly; recode stimulus, apperture movement, and MS movement directions by converting the (og.) numbers into labels (of a sort). Also calculate the normalized angular difference between stimulus und saccade OR stimulus and apperture movement directions. In a third step, calculate average velocities for saccades and apperture movements. Calculate relative differences between stimulus phase shift and saccade as well as phase shift and apperture movement. Label them, too. Finally, recode the participant response (in the different conditions) according to SDT (i.e. have seen vs. havn't seen - FA vs. CoR vs. HIT vs. MISS). Pls find more information on the operations in each line in the margins (this will hold for all following chunks).
```{r}
# Recode experimental condition, session no., and ouput,
# so that calculations/plotting become easier:
mutate( df_raw, condition= factor( conditionNames[ expcon ], levels= conditionNames ),
        vpno   = as.factor( vpno ),
        sesNum = factor( ifelse( str_detect( seno, '01' ), '01', '01<' ), levels= c( '01', '01<' ) ), 
        visResp= factor( case_when( spaKey == 0 ~ 'have_seen', spaKey == 1 ~ 'hvnt_seen' ) ),                  
        phavel = ifelse( str_detect( condition, 'noStim' ), 0, phavel ),
          
# Recalculate angles of stimulus and saccade parameters.
        sacAngleDeg= rad2deg( sacAngle1 ),                                         # - recalculate the saccade angle in degree
        cmbSacN    = ifelse(!str_detect( condition, 'replay' ), sacNumber, 1 ),    # - generate a combined index for generated OR replayed sac. no. 
        cmbAng     = ifelse( str_detect( condition, 'replay' ), 180 - appori, sacAngleDeg ), 
        cmbPkVel   = ifelse( str_detect( condition, 'replay' ), dispSacVPk, sacVPeak ), 
        stioriLab  = ifelse( between( stiori, -90, 90 ), 'right', 'left' ) %>% as.factor,        # - label the direction of the stimulus'  phase shift
        cmbAngLabX = case_when( between( cmbAng, -45, 45 ) ~ 'right', cmbAng < -135 | cmbAng > 135 ~ 'left' ) %>% as.factor,
        cmbAngLabY = ifelse( cmbAng > 0, 'up', 'down' ) %>% as.factor,       
        angDiff    = normalizeRan( absDiffDeg( stiori, cmbAng ) ),  
        relnoMSLab = paste0( cmbSacN, 'MS' ) %>% as.factor,
        relDirsLab = case_when( angDiff <= 45 ~ 'congruent', angDiff >= 135 ~ 'incongruent', TRUE ~ NA_character_ ) %>% as.factor,
        relVelsLab = if_else( between( cmbPkVel, phavel - 5, phavel + 30 ), 'similarVels', 'dissimilarVels' ) %>% as.factor,
        relDsVsLab = ifelse( !is.na( relDirsLab ) & !is.na( relVelsLab ), paste0( relDirsLab, '*', relVelsLab ), NA ) %>% as.factor, 
       
# Recalculate velocities of stimulus and saccade
        PeakVX = cmbPkVel * cos( deg2rad( cmbAng ) ),  # - calculate directional peak velocity of hor. sac component
        PeakVY = cmbPkVel * sin( deg2rad( cmbAng ) ),  # - calculate directional peak velocity of ver. sac component
        dirPhasVel= ifelse( stiori == 180, phavel * -1, phavel ),
        dirPeakVel= ifelse( str_detect( cmbAngLabX, 'right' ), cmbPkVel, cmbPkVel * -1 ),
        retVelVals= dirPeakVel - dirPhasVel,
        retVelsLab= if_else( between( retVelVals, -25, 25 ), 'similarVels', 'dissimilarVels' ),

# Recode response section in SDT terms (for sensitivity and  bias analysis).
        evalSen= case_when( str_detect( condition, 'noStim' ) & visResp == 'hvnt_seen' ~ 'CR',
                            str_detect( condition, 'noStim' ) & visResp == 'have_seen' ~ 'FA',
                           !str_detect( condition, 'noStim' ) & visResp == 'have_seen' ~ 'Hit', 
                           !str_detect( condition, 'noStim' ) & visResp == 'hvnt_seen' ~ 'Miss' ) %>% 
  factor( ., levels= c( 'CR', 'FA', 'Hit', 'Miss' ) ) ) -> df
```

##In vs exclude 1st session.
```{r}
if( str_detect( exclude1st, 'yes' ) ) {
  filter( df, !str_detect( seno, '01|21' ) ) %>% 
    mutate( condition= str_remove_all( condition, 'real_' ) %>% fct_relevel( ., c( 'noStim', 'replay', 'active' ) ) ) -> df }
```

##Double check saccade data.
Make sure that you only include replay conditions trials wo/ microsaccade, and active condition trials w/ microsaccade. Make also sure that the included actice condition trials don't feature microsaccades that should have been excluded bc/ there are either too large (>1 dva) or occured too early/late (before 200 ms or after 800 ms in each trial). Lastly make sure that all further analyses only include trials in which no secondary/tertiary microsaccades have been generated.
```{r}
filter( df, sacNumber > 1 ) %>% select( tID ) -> fltr_2ndMS        # - search for ALL trials w/ 2ndry MS.
group_by( df, vpno, seno, block, trial ) %>%
  filter( str_detect( condition, 'noStim' ) |                      # - accept all no stim trials
          str_detect( condition, 'replay' ) & sacNumber == 0 |     # - accept replay conitions trilas wo/ MS only
          str_detect( condition, 'active' ) & sacNumber <= 1 ) %>% # - acccept active trials /w MS only
  filter( sacNumber == 0 & dispSacAmp <= 1 ||                      # - make sure MS are of appropriat length (>1 dva)
          sacNumber > 0 & sacAmp <= 1 &                           
        ( sacOnset > tStimCf & sacOffset < tStimCd ) ) %>%         #   and appropriate timing.
  filter( !( tID %in% fltr_2ndMS$tID ) ) -> df_ref                 #   filter out ALL trials /w 2ndry MS.
```
      

#Main Analysis.
##Make first analysis of the overall stimulus visibility in the different conditions.
In the first part of this chunk, the data is aggregated per condition. To make it less confision (and, quite frankly, less error prone), each sub-set of conditions is aggregated in its own sub-set of data. In the end, the data is combined and one value per participant and condition is calculated for the sensitivity index d' and the response bias or criterion c. You can find the formulas below:

Formula sensitivity index:

$$ d' =  z( Hit\_rate ) - z( FA\_rate ) $$

Formula decision criterion/bias:

$$ c= -( z( hit\_rate ) + z(FA\_rate ) ) / 2  $$

 'FA__base', 'Hit_active_base', 'Hit_replay_base'


```{r message= FALSE, warning= FALSE}
# Create subs for the data, bind it together, and create evaluation variable.
fHelper <- function( df, cond ) { return( filter( df, !is.na( !!sym( cond ) ) ) %>% mutate( evalCon:= !!sym( cond ) %>% factor ) %>% select( Experiment, vpno, condition, evalCon, evalSen ) ) }

# Calcualt no. of trials and frequency per condtion
bind_rows( fHelper( df_ref, 'relnoMSLab' ), fHelper( df_ref, 'relDirsLab' ), 
           fHelper( df_ref, 'relVelsLab' ), fHelper( df_ref, 'relDsVsLab' ) ) %>%  
  group_by( Experiment, vpno, condition, evalCon, evalSen, .drop= FALSE ) %>%            # - group by relevant factors.
  summarise( nTr= as.double( n() ) ) %>%                                                 # - count no. of trials and no. of saccades.
  filter( condition == 'noStim' &  str_detect( evalSen, 'CR|FA' ) | 
         !condition == 'noStim' & !str_detect( evalSen, 'CR|FA' ),
         !( condition == 'replay' & evalCon == '0MS' ) ) %>%
  mutate_at( 'nTr', ~case_when( .== 0 ~ 1/2, .== sum(.) ~ sum(.) - 1/2, TRUE ~ . ) ) %>% # - count half a trial if fact.-comb. is empty
  group_by( Experiment, vpno, condition, evalCon ) %>% 
  mutate( freq= nTr / sum( nTr ) ) -> df_freq                                            # - calculate frequency

# Join summary data sets and account for 'missing' trials in conditions
filter( df_freq, !str_detect( evalSen, 'CR|Miss' ), !( Experiment == 'JSEP' & vpno != 'y' ) ) %>% 
  pivot_wider( id_cols= c( Experiment, vpno, evalCon ), names_from= c( evalSen, condition ), values_from= freq ) %>% 
  rename_all( ~str_remove(., '_noStim' ) ) %>%
  mutate( dprm_active= qnorm( Hit_active ) - qnorm( FA ), dprm_replay= qnorm( Hit_replay) - qnorm( FA ),
          bias_active= -( qnorm( Hit_active ) + qnorm( FA ) ) / 2, bias_replay= -( qnorm( Hit_replay ) + qnorm( FA ) ) / 2,
          evalCon= factor( evalCon, levels= lev ) ) -> df_indDprm                     
```

##Freqentist statistics
In this chunck, mean values for the no of trials in each condition (f.e. mean no. of FA trials in no-stim condition, mean no of HIT trials in replay condition, etc.) and their frequency are being calculated (f.e. mena CoR rate, mean HIT rate in active contition trials w/ microsaccade, etc.). Additionally, mean values for the sensitivity index (d') and bias (c) are calculated. Together w/ the mean values per condition, the 95% CI is calculated (per condition) as well. In the end, the data is aggregated properly so as to make plotting of the trials/rates/idcs easier. 
```{r warning= F}
# Calculate d' for all participants dependent on experimental and evaluation condition.
pivot_longer( df_indDprm, contains( c( 'FA', 'active', 'replay' ) ), names_to= 'condition' ) %>% 
  filter( str_detect( condition, 'dprm|bias' ) ) %>%
  group_by( Experiment, condition, evalCon ) %>% 
  summarize( mean= mean( value, na.rm= T ), lwrBnd= mean( value, na.rm= T ) - confInt( value ), # - calculte mean d' and lower and upper 
             uprBnd= mean( value, na.rm= T ) + confInt( value ) ) %>%                           # - change back to wide (aggregate data for easy plotting)
  separate( condition, c( 'measure', 'condition' ), sep= '_' ) %>% 
  mutate_at( vars( c( 'mean', contains( 'Bnd' ) ) ), ~ifelse( condition == 'replay', . * -1, . ) ) %>% 
  mutate( condition= as.factor( condition ) ) -> df_sumDprm
```

##Plot sensitiviry index (d') and bias (c).
```{r message= FALSE, warning= FALSE}
# Function for subplotting.
plotIndi <- function( df, m ) { 
  return( filter( df, measure %in% m ) %>%
      mutate( measure= factor( measure, levels= c( 'dprm', 'bias' ) ) ) %>% 
      filter( !is.na( mean ) ) %>% 
      ggplot( mapping= aes( x= mean, y= evalCon, xmin= lwrBnd, xmax= uprBnd, 
                            color= condition:evalCon, fill= condition:evalCon, group= condition ) ) + 
      geom_bar( stat= 'identity', position = 'identity' ) + 
      geom_errorbar( stat= 'identity', position = 'identity', colour= 'black', width= .1 ) +
      scale_color_manual( '', values= cols, aesthetics= c( 'color', 'fill' ), guide= 'none' ) +
      scale_x_continuous( name= '', limits= c( -5, 5 ), breaks= seq( -5, 5 , length.out= 5 ), labels= abs, position= 'top'  ) +
      scale_y_discrete( '', limits= rev ) + 
      facet_grid( Experiment ~ measure, labeller= labeller( measure= c( dprm= 'Sensitivity [d\']', bias= 'Bias [c]' ) ) ) +
      theme( strip.placement= 'outside') -> pout ) }
plotIndi( df_sumDprm, m= c( 'dprm', 'bias'  ) )

# Combine and save.  
if ( savePlots != 'no' ) {
  thematic::thematic_off()
  ggsave( plot= plotIndi( df_sumDprm, m= 'dprm' ) + theme_minimal() + generalTheme,
          path= fld$figures, filename= paste0(  date, '_dp_all', fS$pdf ),
          bg= 'transparent', width= 20.5, height= 13, unit= 'cm', device= cairo_pdf )
  ggsave( plot= plotIndi( df_sumDprm, m= 'bias' ) + theme_minimal() + generalTheme,
          path= fld$figures, filename= paste0(  date, '_c_all', fS$pdf ),
          bg= 'transparent', width= 20.5, height= 13, unit= 'cm', device= cairo_pdf )
  thematic::thematic_on() }
```

##Calculate Pearsons correlation coefficient for the bias (c) and the sensitivity (d') of all participants.
```{r results= 'hide', fig.show= 'hide', message= FALSE, warning= FALSE} 
# Calculate correlations between dprm and bias over different conditions
cor.test( ~dprm_active + dprm_replay, data= filter( df_indDprm, Experiment == 'PSSR' ), method= 'pearson' ) -> corr_DprmPSSR
cor.test( ~bias_active + bias_replay, data= filter( df_indDprm, Experiment == 'PSSR' ), method= 'pearson' ) -> corr_BiasPSSR
cor.test( ~dprm_active + dprm_replay, data= filter( df_indDprm, Experiment == 'CSEP' ), method= 'pearson' ) -> corr_DprmCSEP
cor.test( ~bias_active + bias_replay, data= filter( df_indDprm, Experiment == 'CSEP' ), method= 'pearson' ) -> corr_BiasCSEP
tibble( corr= round( c( corr_BiasPSSR$estimate, corr_DprmPSSR$estimate, NaN, NaN ), 3 ),
        pval= '>0.001', measure= rep( c( 'bias', 'dprm' ), 2 ),  xloc= 1, yloc= 3.75, Experiment= rep( c( 'PSSR','CSEP' ), each= 2 ) ) -> aLayer

# Wrangle data into submission. 
select( df_indDprm, -FA, -contains( 'Hit' ) ) %>%
  filter( evalCon != '0MS' ) %>%
  pivot_longer( contains( c( 'dprm', 'bias' ) ), names_sep= '_', names_to= c( 'measure', 'condition' ) ) %>% 
  pivot_wider( id_cols= c( Experiment, vpno, evalCon, measure ), names_from= condition ) %>% 
  ungroup() -> df_preCirc

# Calculate additional point'coordinates' for semi-circle.
circleFun( select( df_preCirc, contains( c( 'active', 'replay' ) ) ), diameter= .1, npoints= 20, start=  .25, end= 1.25 ) -> df_smCr
circleFun( select( df_preCirc, contains( c( 'active', 'replay' ) ) ), diameter= .1, npoints= 20, start= 1.25, end= 2.25 ) -> df_smCl
 
# Add semi-circle informaton to tibble
ungroup( df_preCirc ) %>%
  mutate( id= 1:n() ) %>% 
  left_join( ., bind_cols( df_smCr, select( df_smCl, -id ) ), by= 'id' ) %>% 
  rename_at( vars( contains( c( 'x..', 'y..' ) ) ), ~c( 'x_active', 'x_replay', 'y_active', 'y_replay' ) ) %>%
  pivot_longer( cols= c( 'x_active', 'x_replay', 'y_active', 'y_replay' ), names_sep= '_', names_to= c( 'coord', 'condition' ), values_to= 'dprm' ) %>%
  pivot_wider( id= c( Experiment:condition ), names_from= coord, values_from= dprm ) %>%
  unnest() %>%
  mutate( condition= as.factor( condition ),
          id= ifelse( str_detect( condition, 'active'), max( id ) + id, id ),
          measure= factor( measure, levels= c( 'dprm', 'bias' ) ) ) %>% 
  left_join( ., aLayer, by= c( 'Experiment', 'measure' ) ) -> df_indCorr

# Plot stuff.
plotIndi2 <- function( df, m ) { 
  return( filter( df, measure %in% m ) %>%
      ggplot() + 
      # geom_smooth( distinct( df_indCorr, active, replay ), mapping= aes( x= replay, y= active ), method= 'lm', inherit.aes= F, alpha= .5 ) +
      geom_smooth( aes( x= ( active ), y= ( replay ) ), method= 'lm', inherit.aes= F, alpha= .5,  ) +
      geom_polygon( aes( x, y, color= condition:evalCon, fill= condition:evalCon, group= id ) ) + 
      geom_text( aes( x= xloc, y= yloc, label= paste( 'ρ[act_rep] ==', corr ) ), parse= TRUE, inherit.aes= FALSE, stat= 'unique', family= 'Calibri', size= 18 *(5/14) ) +
      scale_color_manual( '', values= c( colsRep,  colsAct[2:10] ), aesthetics= c( 'color', 'fill' ) ) +
      scale_x_continuous( 'Active condition', limits= c( -1.5, 4 ), breaks= seq( -1, 4, length.out= 5 ) ) + 
      scale_y_continuous( 'Replay condition', limits= c( -1.5, 4 ), breaks= seq( -1, 4, length.out= 5 ) ) + 
      facet_grid( Experiment ~ measure, labeller= labeller( measure= c( dprm= 'Sensitivity [d\']', bias= 'Bias [c]' ) ) ) +
      theme( legend.position= 'none' ) -> p_corrRaw ) }
plotIndi2( df_indCorr, m= c( 'dprm' ) )

# Combine and save.  
if ( savePlots != 'no' ) {
  thematic::thematic_off()
  ggsave( plot= plotIndi2( df_indCorr, m= c( 'dprm', 'bias' ) ) + theme_minimal() + generalTheme, 
          path= fld$figures, filename= paste0( date, '_pcorr', fS$pdf ),  bg= 'transparent', 
          width= 20.5, height= 13, unit= 'cm', device= cairo_pdf )
  thematic::thematic_on() }
```

