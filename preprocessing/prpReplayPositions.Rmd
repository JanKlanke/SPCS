---
title: "Script to calculate the stimulus positions during the microsaccadic replay."
author: Jan Klanke
date: "`r Sys.Date()`"
output: 
  pdfp_document: default
---


```{r setup, include= FALSE}
knitr::opts_chunk$set( include= TRUE, size= 'footnotesize', message=FALSE, warning=FALSE )
knitr::opts_knit$set(  root.dir= '/Users/jan' )

# Change hook to be able to modify font size in chunks
def.source.hook <- knitr::knit_hooks$get( 'source' )
knitr::knit_hooks$set( source= function( x, options ) {
  x <- def.source.hook( x, options )
  ifelse( !is.null( options$size ), 
         paste0( '\\', options$size, '\n\n', x, '\n\n \\normalsize' ), 
         x ) } )
```

#Preparations.
##Overall parameters.
Here go all the parameters needed for this script: i.e. column names, etc.
```{r}
# Name of the experiment.
expname <- 'CSEP' 

# Names the columns with VP number, session number, block number, and trial number.
identifier <- c( 'vpno', 'seno', 'block', 'trial' )

# Which saccade do you want to look at?
nsac <- 1

# Important eye parameter
trackFreq <- 500
sacMinDur <- 5
sacMaxAmp <- 1

# Important stimulus parameter.
dispFrequency  <- 1440 

# Some switches 
reEvalAll    <- 'no'   # Do you want to rerun all data or just most recent file?
gridSearch   <- 'yes'   # Do you want to run your own grid search or use a fitting function?
statusUpdate <- 'yes'   # Do you want status updates on the % of fitted MS?
```

# Set up functions.
```{r}
# Specify parameter for fitting of RMSE procedure
gamma_function <- function( t, alpha, beta, gamma ) { alpha * ( t / beta )^( gamma - 1 ) * exp( - t / beta )  } # function to fit to velocity profiles: GAMMA
rmse <- list()              # - list for RMSEs per saccade
granLvl <- 50               # - how extensive is the grid supposed to be?

# Function for the main sequence by Collewijn 89.
Collewijn89_func <- function( V0, A0, amp ) { V0 * ( 1 - exp( -amp / A0 ) ) }
Collewijn89_fits <- Collewijn89_fitsRE <- list()

# Function to calculate adequate plot window w/ aspect ratio := 1
limXY <- function( dfp, x, y ) { 
  summarize( dfp, 
             minX= min( !!sym( x ) ), minY= min( !!sym( y ) ), maxX= max( !!sym( x ) ), maxY= max( !!sym( y ) ),
             xDiff= max( abs( !!sym( x ) ) ) - min( abs( !!sym( x ) ) ), yDiff= max( abs( !!sym( y ) ) ) - min( abs( !!sym( y ) ) ),
             mxDiff= max ( max( abs( !!sym( x ) ) ) - min( abs( !!sym( x ) ) ), max( abs( !!sym( y ) ) ) - min( abs( !!sym( y ) ) ) ),
             .groups= 'keep' ) %>% 
    mutate( midX= ( minX + maxX ) / 2, midY= ( minY + maxY ) / 2,
            newminX= midX - mxDiff, newmaxX= midX + mxDiff, newminY= midY - mxDiff, newmaxY= midY + mxDiff ) -> out 
  return( select( out, contains( 'new' ) ) ) }


# select data for plotting (as well as limits for the plotting window).
selectPlotParams <- function( dfp, nsac= '', n= 1, re= 'no' ) {
  nsacOld <- nsac
  if ( nsac == '' || !( nsac %in% unique( dfp$sID ) ) || re == 'yes' ) { unique( dfp$sID )[n] -> nsac ->> nsac }
  filter( dfp, sID %in% nsac ) -> dfp ->> dfp
  if ( nsacOld == '' || !( nsacOld %in% unique( dfp$sID ) ) || re == 'yes' ) { 
    ungroup( dfp ) %>% limXY( 'coorX', 'coorY' ) ->> clims
    ungroup( dfp ) %>% summarize( maxT= max( msec_relSOn ) ) ->> tlims } }
```

# Folder & format spec. parameter
Variables for path to data folder and format specifications. Don't touch unless you use you own folder structure.
```{r}
# Directory of subfolder with data. 
fld <- list()
fld$dataRepo <- './Seafile/Data/'                                        # Directory where the data can be found  
fld$msc <- paste0( fld$dataRepo, expname, '/preprocessed/msCoordinates' ) #  (i.e. './Documents/ExpName/processed/msCoords')
fld$msq <- paste0( fld$dataRepo, expname, '/preprocessed/mainSequence' )  # see above

# Directory of subfolder with functions for MS detection 
fld$functions <- './Documents/CSEP/preprocessing/functions/'   # (i.e. './Documents/ExpName/prepare/functions) 

# Format specs of input and output data (likely no need for change!).
fSpec_csv <- '.csv'  # file format of input/output data
``` 

# Load libraries
These are the libraries that are needed in the script. The libraries are essentially the tidyverse and vroom for fast and efficitne data loading. External functions are not needed before the next step of the data preprocessing ('Preprocessing Script 2: Saccade detection.').
```{r include= FALSE}
# Load libraries.
thematic::thematic_on()
libraries <- c( 'data.table', 'vroom', 'tidyverse', 'RColorBrewer', 'thematic' )
lapply( libraries, require, character.only= TRUE )

sources <- list( 'loopProgress.R' )
map( sources, function( x ) source( paste0( fld$functions, x ) ) )
```

# Load files
Check which files are in the data folder, and list all denotged 'raw' with the correct format spec: '.csv'. Load all listed files in that folder by row. Also: have a first cursory glance on the data.
```{r results= 'hide', message= FALSE, warning= FALSE, fig.align= "center" }
# Get files in target folder.
fList <- list()
list.files( fld$msc, paste0( '*raw*', fSpec_csv ), full.names= TRUE ) -> fList$mscRaw
fList$mscRaw <- fList$mscRaw[1]
if( reEvalAll != 'yes' && !is_empty( fList$mscProcessed ) ) {
  fList$mscRaw <- fList$mscRaw[ !grepl( paste( str_remove( fList$mscProcessed, '_([^_]+)' ), collapse= '|' ), fList$mscRaw ) ] }

# Load data and group by relevant columns.
vroom( fList$mscRaw, col_names= TRUE, delim= ',' ) %>% 
  unite( sID, c( tID, sacNumber ) ) %>% 
  group_by( sID ) %>%
  filter( flag == 'saccade' )  %>%
  mutate_at( vars( contains( 'Idx' ) ), list( org= ~. - .[1] ) ) %>%
  rename_at( vars( contains( 'Idx' ) ), list( ~c( 'msec_relTOn', 'frame_relTOn', 'msec_relSOn', 'frame_relSOn' ) ) ) -> msxy_raw

# Calculate the velocities in the different dimension (horizontal vs. vertical).
mutate_at( msxy_raw, vars( contains( 'coor' ) ), list( vel= ~coalesce( ( . - lag(.) ) * trackFreq, 0 ) ) ) %>%
  rename_at( vars( contains( 'vel' ) ), list( ~c( 'velX', 'velY' ) ) ) -> vel_raw

# Testwise plot of the data
invisible( selectPlotParams( vel_raw, re= 'yes' ) ) 
ggplot( dfp, mapping= aes( x= coorX , y= coorY ) ) + geom_path( color= 'black' ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( . ~ sID, ncol= 1 ) + theme( strip.text= element_text( size= 11 ), aspect.ratio = 1 ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ) ) %>% mutate( name= ifelse( name == 'velX', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, group= name ) ) + geom_path( color= 'black' ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) +
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ) ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) )
```

# Normalize gaze positions
Center saccades on 0 by subtracting the first coordinate pair from all tbat follow.
```{r fig.align= "center"}
# Normalize saccades.
mutate_at( msxy_raw, vars( contains( 'coor' ) ), ~ . - .[1] ) -> msxy_norm

# Calculate the velocities in the different dimension (horizontal vs. vertical).
mutate_at( msxy_norm, vars( contains( 'coor' ) ),  list( vel= ~replace_na( ( . - lag(.) ) * trackFreq, 0 ) ) ) %>%
  rename_at( vars( contains( 'vel' ) ), ~c( 'velX', 'velY' ) ) -> vel_norm

# Calcualte plot specifics for x and y achsis length.
invisible( selectPlotParams( vel_norm, nsac, re= 'yes' ) )
ggplot( dfp, mapping= aes( x= coorX, y= coorY ) ) + geom_path( color= 'black' ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( . ~ sID ) + theme( strip.text= element_text( size= 11 ), aspect.ratio= 1 ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ) ) %>% mutate( name= ifelse( name == 'velX', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, group= name ) ) + geom_path( color= 'black' ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) +
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ) ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) )
```

# Filter by amplitude
Cut off those samples of the microsaccade that follow after the maximal amplitude is reached in either(!) the x or y dimension.
```{r fig.align= "center"}
# Kick out ms sample after max amplitde is reached
group_by( vel_norm, sID ) %>% 
  slice( 1: min( c( which.max( abs( coorX ) ), which.max( abs( coorY ) ) ) ) ) %>%
  mutate_at( vars( matches( c( 'coorX', 'coorY' ) ) ), list( maxA= ~last(.) - first(.) ) ) %>%                   # - recalculate max amplitude of combined 
  rename_at( vars( contains( 'maxA' ) ), list( ~c( 'maxAmpX', 'maxAmpY' ) ) ) %>% 
  filter( sqrt( maxAmpX^2 + maxAmpY^2 ) < 1 ) -> vel_ampFiltered                                                 # - rearrange to make tibble more compellin
 
# Plot stuff.
invisible( selectPlotParams( vel_ampFiltered, nsac ) )
ggplot( dfp, mapping= aes( x= coorX , y= coorY ) ) + geom_path( color= 'black' ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( vars( sID ) ) + theme( strip.text= element_text( size= 11 ), aspect.ratio = 1 ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ) ) %>% mutate( name= ifelse( name == 'velX', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, group= name ) ) + geom_path( color= 'black'  ) + geom_point( color= 'red' ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) +
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ) ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) ) 
```

# Filter saccades that are too short.
If, by amplitude filtering, the remaining saccades shorter than 5 ms, they need to be kicked to the curb.
```{r}
# Filter out saccades that are too short or exceed max amplitude.
group_by( vel_ampFiltered, sID ) %>% 
  filter( sum( flag == 'saccade' ) > ceiling( sacMinDur / ( 1000 / trackFreq ) ) ) -> vel_lenFiltered
```

# Project real eye positions on saccade vector
Calculate the relative position of each sample during the saccade relative to the saccade amplitude. It might be easier to think of this as the percentage 'copmpleted' of the microsaccade (eye position in each sample rel. microsaccade length). In a second step, project these relative position values on the artificially created, 'simplified' saccade vector. Values for the saccade vector are plotted in blue and with dashed line in the following.
```{r fig.align= "center"}
rowwise( vel_lenFiltered ) %>%                                                                                    #  - as as grouping by sID, sac number, AND msec Id  
  mutate( relPos= c( coorX, coorY ) %*% c( maxAmpX, maxAmpY ) / sqrt( ( maxAmpX )^2 + ( maxAmpY )^2 )^2,          # - calculate 'coordinate / full vector' (i.e. % done)
          coorPX= relPos %*% ( maxAmpX ), coorPY= relPos %*% ( maxAmpY ) ) %>%                                    # - calculate simpülified X and Y coordinates
  group_by( sID ) %>% 
  mutate_at( vars( matches( c( 'coorPX', 'coorPY' ) ) ), 
             list( velP= ~replace_na( ( . - lag(.) ) * trackFreq , 0 ) ) ) %>%
  rename_at( vars( contains( 'velP' ) ), list( ~c( 'velPX', 'velPY' ) ) ) -> vel_projected

# Plot stuff.
invisible( selectPlotParams( vel_projected, nsac ) )
pivot_longer( dfp, contains( 'coor' ), names_pattern= '(.*)(.{1})$', names_to= c( 'group', '.value' ) ) %>%
  ggplot( mapping= aes( x= X , y= Y, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( vars( sID ) ) + theme( aspect.ratio = 1, strip.text= element_text( size= 11 ), legend.position= 'none'  ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ) ) %>% separate( name, c( 'group', 'name' ), sep= -1 ) %>%
  mutate( name= ifelse( name == 'X', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) +
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( legend.position= 'none', strip.placement= 'outside', strip.text= element_text( size= 11 ) ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) ) 
```

# Fitting a gamma function to the velocity profile of the saccade vector
In the following chunk, a gamma function is fitted to the velocity values of the projected(!) 1D saccade vector. The fitting is done as a grid search of the parameterspace alpha: 0-100, beta: .1-5, gamma: 1-20, by means of a grid search. Optimal fits are determined by means of a root mean square error (RMSE) procedude. Fitted velocity curves of 5 exemplary trials are depicted in green and dotted lines in the following, as are the RMSE values for these trials.
```{r fig.align= "center"}
# Prepare data for RMSE: calculate 1D gaze position vector for real and projected saccade data (dva)
# and calcualte the velocities along these two vectors (dva/s).
mutate( vel_projected, 
        coorXY= sqrt( coorX^2 + coorY^2 ), coorPXY= sqrt( coorPX^2 + coorPY^2 ), 
        velXY= replace_na( ( coorXY - lag( coorXY ) ) * trackFreq, 0 ), 
        velPXY= replace_na( ( coorPXY - lag( coorPXY ) ) * trackFreq, 0 ) ) -> vel_model


# IF you want to make a grid search; create matrix with parameter combinations.
if( gridSearch == 'yes' ) {  
  tibble( alpha= seq( 0, 100, length.out= granLvl ),
          beta=  seq( .1, 5, length.out= granLvl ),
          gamma= seq( 1, 20, length.out= granLvl ) ) %>% 
    expand.grid() -> parameterGrid
}

# Loop over saccades to calculate RMSE for each set of parameters per saccade.
for( nsacl in 1: nrow( distinct( vel_model, sID ) ) ) {
  
  # Extract individual saccades.
  filter( vel_model, sID == distinct( vel_model, sID )[nsacl,]$sID ) -> dfp 
  
  # Again, IF you want to make a grid search:
  if( gridSearch == 'yes' ) {  
    # Reshape parameter matrix and time data into tidy tibble.
    tibble( msec= rep( dfp$msec_relSOn, nrow( parameterGrid ) ), 
            alpha= rep( parameterGrid$alpha, each= length( dfp$msec_relSOn ) ), 
            beta= rep( parameterGrid$beta, each= length( dfp$msec_relSOn ) ),
            gamma= rep( parameterGrid$gamma, each= length( dfp$msec_relSOn ) ) ) %>%
      mutate( velMXY= gamma_function( msec, alpha, beta, gamma ) ) -> dfp_model
    
   # Do the rmse procedure.
   dfp[ rep( seq_len( nrow( dfp ) ), nrow( parameterGrid ) ), ] %>%
     bind_cols( dfp_model ) %>% 
     mutate( error= ( velMXY - velPXY ) ) %>%# - importantly: Calculate difference between modelled and PROJECTED(!) data 
     group_by( alpha, beta, gamma ) %>% 
     summarize( sID= unique( sID ), rmse= sqrt( sum( error^2 ) / nrow( dfp ) ), .groups= 'keep' ) %>% 
     group_by( sID ) %>% 
     filter( rmse == min( rmse ) ) %>%
     group_by( sID ) %>% 
     bind_rows( rmse, . ) -> rmse
   
   # If you don't want to make your own grid seach but use the Levenberg-Marquardt algorithm for automatic fitting...:
  } else {
      minpack.lm::nlsLM( dfp, formula= velPXY ~ gamma_function( msec_relSOn, alpha, beta, gamma ),
        start= c( alpha= 50, beta= 2.5, gamma= 4 ) ) -> model
      tibble( sID= distinct( dfp, sID )$sID,  
              alpha= coef( model )[1], 
              beta = coef( model )[2], 
              gamma= coef( model )[3],
              rmse= sqrt( deviance(model ) / nrow( dfp ) ) ) %>% 
        bind_rows( rmse, .) -> rmse
  }
  
  # Counter.
  loopProgress( nsacl, nrow( distinct( vel_model, sID ) ), 5 )
}

# Recombine data.
left_join( vel_model, rmse, by= 'sID' ) %>% 
  mutate( velMXY= gamma_function( msec_relSOn, alpha, beta, gamma ) ) -> vel_fit_gamma

# Plot stuff.
invisible( selectPlotParams( vel_fit_gamma, n=16:20, re= 'yes' ) )
pivot_longer( dfp, contains( 'coor' ), names_pattern= '(.*)(.{1})$', names_to= c( 'group', '.value' ) ) %>% drop_na() %>%
  ggplot( mapping= aes( x= X , y= Y, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( vars( sID ), ncol= 1 ) + theme( strip.text.x = element_blank(), legend.position= 'none' ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ), names_to= 'group' ) %>% mutate( group= factor( group, levels= c( 'velXY', 'velPXY', 'velMXY' ) ) ) %>% drop_na() %>% 
  ggplot( mapping= aes( x= msec_relSOn, y= value, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue', 'green' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Time [ms]', ) + scale_y_continuous( 'Velocity along 1D sac. vec. [dva/s]' ) +
  facet_grid( rows= vars( sID )  ) + 
  theme( strip.text.x = element_blank() , legend.position= 'none' ) -> p2
ggplot( dfp, mapping= aes( x= rmse, factor( sID, level= sort( unique( dfp$sID ), decreasing= TRUE ) ) ) ) +
  geom_point( color= 'green' ) + 
  scale_x_continuous( 'RSME', limits= c( 0, max( vel_fit_gamma$rmse ) ) ) + scale_y_discrete( '', labels= '' ) + 
  facet_wrap( .~sID, scales= 'free_y', ncol= 1, strip.position= 'right' ) -> p_rmse
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ggplotGrob( p_rmse ), ncol= 3 )
```

# Check RSME and filter for terrible fits.
In the following chunk, the RMSEs of all fits are compared and fits that are too bad (i.e. RMSE > mean RMSE + 2 * standard deviation ) are excluded. In the end, all RMSEs are ploted. To get an idea whether the exclusion(s) was(were) necessary, the EXCLUDED fits will be displayed as well.
```{r}
# Calculate crucial RSME stats and filter by them.
ungroup( vel_fit_gamma ) %>% summarize( mean= mean( rmse, na.rm= T ), sd= sd( rmse, na.rm= T ), cutoffCrit= mean + 3 * sd ) -> sRMSE
filter( vel_fit_gamma, rmse < sRMSE$cutoffCrit ) -> vel_rsmeFltrd

# Plot stuff.
bind_cols( vel_rsmeFltrd, sRMSE ) -> dfp 
brewer.pal( name= 'Greys', 9 ) -> exCol
brewer.pal( name= 'Spectral', 11 ) -> bgCol
ggplot( dfp ) +
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= mean - sd * 3, ymax= mean + sd * 3 ), fill= exCol[9],   alpha= .5, stat= 'unique' ) +
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= mean - sd * 2, ymax= mean + sd * 2 ), fill= exCol[4.5], alpha= .5, stat= 'unique' ) +
  geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= mean - sd,     ymax= mean + sd     ), fill= exCol[1],   alpha= .5, stat= 'unique' ) +
  geom_hline( aes( yintercept= mean ), linetype= 'dashed' ) +
  geom_hline( aes( yintercept= cutoffCrit ), color= 'red' ) + 
  geom_label( aes( x= unique( sID )[length( unique( sID ) ) / 2 ],
                   y= cutoffCrit, label= 'Cut off crit.' ), color= 'red', stat= 'unique' ) +
  geom_point( mapping= aes( x= sID, y= rmse, color= rmse ) ) +
  scale_color_distiller( name= '', palette= 'Spectral' ) + 
  scale_x_discrete( '' ) + scale_y_continuous( 'RMSE [dva/s]' ) +
  theme( axis.text.x= element_text( size= 6, angle= 90, hjust= 1, vjust= 1 ), legend.position= 'none' ) -> p_rmse
if( filter( vel_fit_gamma, rmse > sRMSE$cutoffCrit ) %>% nrow() > 0 ) {
  pivot_longer( vel_fit_gamma, cols= contains( 'vel' ), names_to= 'group' ) %>% 
    filter( rmse > sRMSE$cutoffCrit, str_detect( group, 'XY' ) ) %>% 
    ggplot() + geom_rect( aes( xmin= -Inf, xmax= Inf, ymin= -Inf, ymax= Inf, fill= rmse ), alpha= .15 ) +
    scale_fill_distiller( name= '', palette= 'Spectral', limits= c( min( vel_fit_gamma$rmse ), max( vel_fit_gamma$rmse ) ) ) + 
    geom_path( aes( x= msec_relSOn, y= value, linetype= group ), color= 'black') + geom_point( aes( x= msec_relSOn, y= value, color= group ) ) + 
    scale_x_continuous( 'Time [ms]' ) + scale_y_continuous( 'Velocity along 1D sac. vec. [dva/s]' ) +
    theme( legend.position= 'none' ) + facet_wrap( vars( sID ), ncol= 1 ) -> p_spec
  gridExtra::grid.arrange( ggplotGrob( p_rmse ), ggplotGrob( p_spec ), ncol= 2 )
} else { gridExtra::grid.arrange( ggplotGrob( p_rmse ), ncol= 1 ) }
```

# Recalculate real gaze positions based on the velocity fits
Extract the x- and y- coordinates of the fitted velocities by (a) calculating the cumulative sum of the fitted velocities, (b) dividing these cum. sums by the full sum of the velocities to generate the incremental relative velocity gain, and (c) multiplying the result with the max amplitude of the saccade to be fitted.
NB: In the plot of the gaze coordinates, the projected coordinates are omitted due to the heavy overlap with the modelled values.
```{r fig.align= "center"}
mutate( vel_rsmeFltrd, relVel= cumsum( velMXY ) / sum( velMXY ), 
        coorMX= relVel * maxAmpX, coorMY= relVel * maxAmpY ) %>%
  mutate_at( vars( contains( 'coorM' ) ), list( velMM= ~replace_na( ( . - lag(.) ) * trackFreq, 0 ) ) ) %>%
  rename_at( vars( contains( 'velMM' ) ), list( ~c( 'velMX', 'velMY' ) ) ) -> msxy_fit_gamma

# Plot stuff.
invisible( selectPlotParams( msxy_fit_gamma, n= 1 ) ) 
select( dfp, -contains( 'XY' ) ) %>% pivot_longer( contains( 'coor' ), names_pattern= '(.*)(.{1})$', names_to= c( 'group', '.value' ) ) %>%
  mutate( group= factor( group, levels= c( 'coor', 'coorP', 'coorM' ) ) ) %>%
  ggplot( aes( x= X , y= Y, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue', 'green' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( .~ sID ) + theme( aspect.ratio = 1, strip.text= element_text( size= 11 ), legend.position= 'none' ) -> p1
select(  dfp, -contains( 'XY' ) ) %>% pivot_longer( cols= starts_with( 'vel' ) ) %>% separate( name, c( 'group', 'name' ), sep= -1 ) %>% 
  mutate( group= factor( group, levels= c( 'vel', 'velP', 'velM' ) ),
          name= ifelse( name == 'X', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue', 'green' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) + 
  facet_wrap( sID ~ name, ncol= 1, strip.position= 'left' ) + 
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ), legend.position= 'none' ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) ) 
```

# Upsampling mechanism
Because the sampling frequency of the eyetracker differs from the refresh rate of the screen (500 Hz vs 1440 Hz) - the saccade vector needs to be upsampled. This is done by means of the gamma function: Instead of clculating the x and y coordinates for the number of saccade samples (generated by the eye tracker at 500 Hz), the gamma function is used to calcuate the x and y coordinates for the number of screen refreshs for each MS. (In the plots, the real positions and velocities are extrapolated linearly for a better comparison). 
NB: In the plot of the gaze coordinates, the projected coordinates are omitted due to the heavy overlap with the modelled values.
```{r fig.align= "center"}
# Function for upsampling of tracking (500 hz) to display frequency (1440 hz).
group_by( msxy_fit_gamma, sID ) %>%
  summarize( frame_relSOn= min( frame_relSOn ): max( frame_relSOn ), 
             frame_relTOn= min( frame_relTOn ): max( frame_relTOn ) ) %>% 
  merge( msxy_fit_gamma, by= c( 'sID', 'frame_relSOn', 'frame_relTOn' ), all= TRUE ) %>% 
  fill( c( 'sID', all_of( identifier ), 'flag',
           contains( 'max' ), 'alpha', 'beta', 'gamma', 'rmse' ), .direction= 'down' ) %>%
  mutate( msec_relSOn= frame_relSOn * 1 / dispFrequency * 1000,
          msec_relTOn= frame_relTOn * 1 / dispFrequency * 1000 ) -> vel_pre_upsampled

# Calculate the velocities in the different dimension (horizontal vs. vertical).
group_by( vel_pre_upsampled, sID ) %>% 
  select( -contains( 'velM' ), 'relVel' ) %>%
  mutate( velMXY= gamma_function( msec_relSOn, alpha, beta, gamma ),
          relVel= cumsum( velMXY ) / sum( velMXY ), 
          coorMX= relVel * maxAmpX, coorMY= relVel * maxAmpY ) %>%
  mutate_at( vars( contains( 'coorM' ) ), list( velMM= ~replace_na( ( . - lag(.) ) * dispFrequency, 0 ) ) ) %>%
  rename_at( vars( contains( 'velMM' ) ), list( ~c( 'velMX', 'velMY' ) ) ) -> vel_upsampled
  
# Plot stuff.
invisible( selectPlotParams( vel_upsampled ) )
select( dfp, -contains( 'XY' ) ) %>% pivot_longer( contains( 'coor' ), names_pattern= '(.*)(.{1})$', names_to= c( 'group', '.value' ) ) %>% drop_na( X ) %>%
  mutate( group= factor( group, levels= c( 'coor', 'coorP', 'coorM' ) ) ) %>% select( - contains( 'vel' ), -'relPos') %>%
  ggplot( aes( x= X, y= Y, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue', 'green' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( ~ sID ) + theme( aspect.ratio = 1, strip.text= element_text( size= 11 ), legend.position= 'none' ) -> p1
select(  dfp, -contains( 'XY' ) ) %>% pivot_longer( cols= starts_with( 'vel' ) ) %>% separate( name, c( 'group', 'name' ), sep= -1 ) %>% drop_na( value ) %>%
  mutate( group= factor( group, levels= c( 'vel', 'velP', 'velM' ) ), 
          name= ifelse( name == 'X', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, color= group, fill= group, linetype= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'blue', 'green' ), aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'Time [ms]', limits= c( 0, tlims$maxT ) ) + scale_y_continuous( '' ) + 
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ), legend.position= 'none' ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) )
```

# Calculate main sequence parameter for fitted data
```{r}
# Create a data tibble with data necessary fot the fit.
group_by( vel_upsampled, sID ) %>%
  mutate( sacVPeak= max( velMXY ) ) %>%
  distinct( vpno, seno, sacVPeak, maxAmpX, maxAmpY ) %>% 
  mutate( sacAmp= sqrt( maxAmpX^2 + maxAmpY^2 ) ) %>% 
  group_by( vpno, seno ) -> Collewijn89_data
  
# Fit main sequence function to data (per vpno and session),
for( ns in group_indices( Collewijn89_data ) %>% unique() ) {
  filter( Collewijn89_data, group_indices() == ns ) -> data
    minpack.lm::nlsLM( data, formula= sacVPeak ~ Collewijn89_func( V0, A0, sacAmp ),
      start= c( V0= 7.9, A0= 450 ) ) -> Collewijn89_coef
    
    # Create a tibble with vpno, seno, and coeficients.
    tibble( vpno= distinct( data, vpno )$vpno, seno= distinct( data, seno )$seno, 
            V0= coef( Collewijn89_coef )[1], A0= coef( Collewijn89_coef )[2] ) -> Collewijn89_fits[[ns]]
}

# FExtraxt values of amplitude to lower quantile.
mutate( Collewijn89_data, maxAmp= sqrt( maxAmpX^2 + maxAmpY^2 ) ) %>% 
  group_by( seno ) %>% do( broom::tidy( t( quantile( .$maxAmp ) ) ) ) -> Collewijn89_ampQuant
bind_rows( Collewijn89_fits ) %>% bind_cols( ampVal= Collewijn89_ampQuant$X25. ) -> Collewijn89_fit

# Plot stuff.
tibble( sID= rep( Collewijn89_data$sID, each= 20 ), 
        vpno= rep( Collewijn89_data$vpno, each= 20 ), 
        seno= rep( Collewijn89_data$seno, each= 20 ),
        sacAmp_fit= rep( seq( 0, 1, length.out= 20 ), nrow( distinct( Collewijn89_data, sID ) ) ) ) %>%
  left_join( Collewijn89_data, by= c( 'sID', 'vpno', 'seno' ) ) %>%
  left_join( Collewijn89_fit, by= c( 'vpno', 'seno' ) ) %>% 
  mutate( sacVPeak_fit= Collewijn89_func( V0, A0, sacAmp_fit ) ) %>% 
  group_by( vpno, seno ) -> dfpMS1
ggplot( dfpMS1 ) + geom_point( aes( x= sacAmp, y= sacVPeak, color= as.factor( seno ) ) ) +
  geom_hline( aes( yintercept= Collewijn89_func( V0, A0, ampVal ) ), color= 'red' ) +
  scale_color_brewer( type= 'qual' , aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'MS amplitude [dva]' ) + scale_y_continuous( 'MS velocity [dva/s]' ) +
  geom_line( aes( x= sacAmp_fit, y= sacVPeak_fit) ) +
  geom_point( aes( x= ampVal, y= Collewijn89_func( V0, A0, ampVal ) ), color= 'red', size= 2 ) +
  labs( color= 'Session no.' ) + theme( legend.position= 'bottom' )
```

# Check microsaccade on- and offsets
To ensure that the microsaccades to be replayed stareted and ended within the time window during which the stimulus will be at full contrast, this chunk will exclude all microsaccades that started earlier than 200 ms and ended later than 800 ms.
```{r}
# Because the upsampling happens after the fitting, the upsampling sometime reveals that the fitted curves
# have a vastly different shape than the fitted curves suggest. Therefore, this filter makes sure that the peak
# velocities are biologically plausible.
mutate( Collewijn89_fit, maxVPeak= Collewijn89_func( V0, A0, 1 ), cutoffCrit= 3 * maxVPeak ) -> sVPeak
group_by( vel_upsampled, sID ) %>% 
  summarize( peakV = max( velMXY ) ) %>% 
  filter( peakV < sVPeak$cutoffCrit ) %>% 
  select( sID ) -> fltr_v

# Check onset and offset times of the MS and filter those outside of time window during
# which the stimulus was at full contrast.
group_by( vel_upsampled, sID ) %>% 
  slice( 1, n() ) %>%
  filter( msec_relTOn < 200 | msec_relTOn > 800 ) %>%
  distinct( sID ) -> fltr_t

# Plot stuff.
summarize( vel_upsampled, vPeak= max( velMXY ) ) %>%
  ggplot() + 
  geom_rect( data= sVPeak, aes( xmin= -Inf, xmax= Inf, ymin= 0, ymax= maxVPeak * 3 ), fill= exCol[9],   alpha= .5 ) +
  geom_rect( data= sVPeak, aes( xmin= -Inf, xmax= Inf, ymin= 0, ymax= maxVPeak * 2 ), fill= exCol[4.5], alpha= .5 ) +
  geom_rect( data= sVPeak, aes( xmin= -Inf, xmax= Inf, ymin= 0, ymax= maxVPeak     ), fill= exCol[1],   alpha= .5 ) +
  geom_hline( data= sVPeak, aes( yintercept= maxVPeak   ), linetype= 'dashed' ) +
  geom_hline( data= sVPeak, aes( yintercept= cutoffCrit ), color= 'red' ) + 
  geom_point( mapping= aes( x= sID, y= vPeak, color= vPeak ) ) +
  geom_label( mapping= aes( x= length( unique( vel_upsampled$sID ) ) / 2, y= sVPeak$cutoffCrit, label= 'Cut off crit.' ), color= 'red' ) +
  scale_color_distiller( palette= 'Spectral' ) + 
  scale_y_continuous( 'RMSE [dva/s]' ) +
  theme( axis.text.x= element_text( size= 6, angle= 90, hjust= 1, vjust= 1 ), 
        legend.position= 'none', axis.title.x = element_blank() ) -> p1

slice( vel_upsampled, 1, n() ) %>%
  mutate( lab= c( 'onset', 'offset' ) ) %>% 
  pivot_wider( id_cols= sID, names_from= lab, values_from= msec_relTOn ) %>%
  mutate( fltr= ifelse( sID %in% fltr_t$sID, 'out', 'in' ) ) %>%
  ggplot( aes( xmin= onset, xmax= offset, y= sID, color= fltr ) ) +
  geom_vline( xintercept= c( 200, 800 ), color= 'red', linetype= 'dashed' ) + 
  geom_label( mapping= aes( x= 200, y= sID[3], label= 'Cut off crit.' ), color= 'red' ) +
  geom_label( mapping= aes( x= 800, y= sID[3], label= 'Cut off crit.' ), color= 'red' ) +
  geom_linerange() + 
  scale_color_manual( values= brewer.pal( 9, 'RdYlBu' )[c( 9,3 )] ) + 
  scale_x_continuous( 'Time [ms]' ) + 
  theme( axis.text.y= element_text( size= 6 ), axis.title.y = element_blank(),
         legend.position= 'none' ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), ncol= 2 )


# Do the actual filtering.
filter( vel_upsampled, sID %in% fltr_v$sID, !( sID %in% fltr_t$sID ) ) -> vel_upsampled_fltr
```

# Re-fit the data to the main sequencee
```{r}
# Create a data tibble with data necessary fot the fit.
group_by( vel_upsampled_fltr, sID ) %>%
  mutate( sacVPeak= max( velMXY ) ) %>%
  distinct( vpno, seno, sacVPeak, maxAmpX, maxAmpY ) %>% 
  mutate( sacAmp= sqrt( maxAmpX^2 + maxAmpY^2 ) ) %>% 
  group_by( vpno, seno ) -> Collewijn89_dataRE
  
# Fit main sequence function to data (per vpno and session),
for( ns in group_indices( Collewijn89_dataRE ) %>% unique() ) {
  filter( Collewijn89_dataRE, group_indices() == ns ) -> data
    minpack.lm::nlsLM( data, formula= sacVPeak ~ Collewijn89_func( V0, A0, sacAmp ),
      start= c( V0= 7.9, A0= 450 ) ) -> Collewijn89_coefRE
    
    # Create a tibble with vpno, seno, and coeficients.
    tibble( vpno= distinct( data, vpno )$vpno, seno= distinct( data, seno )$seno, 
            V0= coef( Collewijn89_coefRE )[1], A0= coef( Collewijn89_coefRE )[2] ) -> Collewijn89_fitsRE[[ns]]
}

# FExtraxt values of amplitude to lower quantile.
mutate( Collewijn89_dataRE, maxAmp= sqrt( maxAmpX^2 + maxAmpY^2 ) ) %>% 
  group_by( seno ) %>% do( broom::tidy( t( quantile( .$maxAmp ) ) ) ) -> Collewijn89_ampQuantRE
bind_rows( Collewijn89_fitsRE ) %>% bind_cols( ampVal= Collewijn89_ampQuant$X25. ) -> Collewijn89_fitRE

# Plot stuff.
tibble( sID=  rep( Collewijn89_dataRE$sID, each= 20 ), 
        vpno= rep( Collewijn89_dataRE$vpno, each= 20 ), 
        seno= rep( Collewijn89_dataRE$seno, each= 20 ),
        sacAmp_fit= rep( seq( 0, 1, length.out= 20 ), nrow( distinct( Collewijn89_dataRE, sID ) ) ) ) %>%
  left_join( Collewijn89_dataRE, by= c( 'sID', 'vpno', 'seno' ) ) %>%
  left_join( Collewijn89_fitRE, by= c( 'vpno', 'seno' ) ) %>% 
  mutate( sacVPeak_fit= Collewijn89_func( V0, A0, sacAmp_fit ) ) %>% 
  group_by( vpno, seno ) %>% 
  mutate( data_set= '(b) after_filtering' ) -> dfpMS2

mutate( dfpMS1, data_set= '(a) before_filtering' ) %>% 
  bind_rows( ., dfpMS2 ) %>% 
  ggplot() + geom_point( aes( x= sacAmp, y= sacVPeak, color= as.factor( seno ) ) ) +
  geom_hline( aes( yintercept= Collewijn89_func( V0, A0, ampVal ) ), color= 'red' ) +
  scale_color_brewer( name= 'Session no.',  type= 'qual' , aesthetics= c( 'color', 'fill' ) ) +
  scale_x_continuous( 'MS amplitude [dva]' ) + scale_y_continuous( 'MS velocity [dva/s]' ) +
  geom_line( aes( x= sacAmp_fit, y= sacVPeak_fit) ) +
  geom_point( aes( x= ampVal, y= Collewijn89_func( V0, A0, ampVal ) ), color= 'red', size= 2 ) +
  facet_wrap( vars( data_set ), scales= 'free_y' ) + theme( legend.position= 'bottom', strip.text= element_text( size= 11 ) ) 
```

# Transform into retinal consequence.
```{r fig.align= "center"}
# "Mirror" saccades.
mutate_at( vel_upsampled_fltr, vars( contains( 'coorM' ) ), list( retCon = ~ . * -1 ) ) %>% 
  rename_at( vars( contains( 'retCon' ) ), ~c( 'coorRCX', 'coorRCY' ) ) -> rcxy_upsampled_fltr

# Recalculate velocities (only for display purposes really).
mutate_at( rcxy_upsampled_fltr, vars( contains( 'coorRC' ) ),
           list( vel= ~replace_na( ( . - lag(.) ) * dispFrequency, 0 ) ) ) %>%
  rename_at( vars( contains( '_vel' ) ), ~c( 'velRCX', 'velRCY' ) ) -> rcvel_upsampled_fltr

# Plot stuff.
invisible( selectPlotParams( rcvel_upsampled_fltr, n= 1:5, re= 'yes' ) )
pivot_longer( dfp, contains( 'coor' ), names_pattern= '(.*)(.{1})$', names_to= c( 'group', '.value' ) ) %>% 
  filter( str_detect( group, 'coorM|coorRC' ) ) %>%
  ggplot( mapping= aes( x= X , y= Y, color= group, fill= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'purple' ), aesthetics= c( 'color', 'fill' ), guide= F ) +
  scale_x_continuous( 'Hor. gaze position [dva]',  limits= c( clims$newminX, clims$newmaxX ) ) + 
  scale_y_continuous( 'Vert. gaze position [dva]', limits= c( clims$newminY, clims$newmaxY ) )  +
  facet_wrap( vars( sID ), ncol= 1 ) + theme( strip.text.x= element_blank() ) -> p1
pivot_longer( dfp, cols= contains( 'vel' ) ) %>% filter( str_detect( name, 'velM|velRC' ), !str_detect( name, 'XY' ) ) %>%
  separate( name, c( 'group', 'name' ), sep= -1 ) %>%
  mutate( name= ifelse( name == 'X', 'Hor.\ velocity [dva/s]', 'Vert.\ velocity [dva/s]' ) ) %>%
  ggplot( mapping= aes( x= msec_relSOn, y= value, color= group, fill= group ) ) + geom_path( color= 'black' ) + geom_point() +
  scale_color_manual( name= '', values = c( 'red', 'purple' ), aesthetics= c( 'color', 'fill' ), guide= F) +
  scale_x_continuous( 'Time [ms]', ) + scale_y_continuous( '' ) +
  facet_grid( cols= vars( sID ), rows= vars( name ), switch= 'y'  ) + 
  theme( strip.placement= 'outside', strip.text= element_text( size= 11 ) ) -> p2
gridExtra::grid.arrange( ggplotGrob( p1 ), ggplotGrob( p2 ), widths= c( 1.5, 1 ) )
```


# Prepare file for output
Bring data into proper shape for output files. If necessary, convert sID back into meaningful columns. Store the processed data as a csv in a special subfolder.
```{r}
# Split sID into more informative columns.
separate( rcvel_upsampled_fltr, col= sID, into= c( 'tID', 'sacNumber' ), sep= '_' ) %>% 
  separate( col= tID, into= c( identifier ), sep= c( 1, 3, 5 ), remove= FALSE ) %>% 
  select( all_of( identifier ), sacNumber, contains( c( 'msec', 'coorM', 'velM', 'RC', 'maxAmp' ) ) ) -> vel_upsampled_fin

# Make sure that--if you save--really only the fit of the first session of each participant is saved.
group_by( Collewijn89_fitRE, vpno ) %>% filter( seno == '01' ) -> Collewijn89_fin

# Create filename & save output for ms coords: 
paste0( fld$msc, '/', unique( vel_upsampled_fin$vpno ), max( vel_upsampled_fin$seno, na.rm= TRUE ), '_msCoords', fSpec_csv ) -> fList$msCoords
vroom_write( x= vel_upsampled_fin, path= fList$msCoords, delim= ',', col_names= TRUE )

# Create filename & save output for ms coords IF its the first session of the participant:
if( dim( Collewijn89_fin )[1] > 0 ) {
  paste0( fld$msq, '/', unique( Collewijn89_fin$vpno ), '_mSequ', fSpec_csv ) -> fList$mainSequence
  vroom_write( x= Collewijn89_fin,   path= fList$mainSequence, delim= ',', col_names= TRUE )
}

# Push raw files in new subfolder after processing
fs::file_move( fList$mscRaw, paste0( fld$msc, '/raw' ) )
```